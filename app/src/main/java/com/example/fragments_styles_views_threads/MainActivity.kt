package com.example.fragments_styles_views_threads

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.fragments_styles_views_threads.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)
    }


}
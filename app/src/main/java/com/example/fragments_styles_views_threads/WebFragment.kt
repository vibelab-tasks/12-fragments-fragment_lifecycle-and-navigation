package com.example.fragments_styles_views_threads

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.fragments_styles_views_threads.databinding.FragmentWebBinding
import com.google.android.material.bottomnavigation.BottomNavigationView

class WebFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentWebBinding.inflate(layoutInflater)
        val bottomNavigationView: BottomNavigationView = binding.bottomNavigationView
        val view = binding.root

        bottomNavigationView.setOnItemSelectedListener { menuItem ->
            if (menuItem.itemId == R.id.mi_home)
                Navigation.findNavController(view).navigate(R.id.navigateToHomeFragment)

            true
        }

        return view
    }

    companion object {
        fun newInstance() =
            WebFragment()
    }
}
package com.example.fragments_styles_views_threads

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.fragments_styles_views_threads.databinding.FragmentHomeBinding
import com.google.android.material.bottomnavigation.BottomNavigationView

// TODO: Rename parameter arguments, choose names that match

class HomeFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val binding = FragmentHomeBinding.inflate(layoutInflater)
        val bottomNavigationView: BottomNavigationView = binding.bottomNavigationView
        val view = binding.root

        bottomNavigationView.setOnItemSelectedListener { menuItem ->
            if (menuItem.itemId == R.id.mi_web)
                Navigation.findNavController(view).navigate(R.id.navigateToWebFragment)

            true
        }

        return view
    }

    companion object {

        fun newInstance() =
            HomeFragment()
    }
}
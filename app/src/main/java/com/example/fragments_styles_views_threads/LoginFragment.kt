package com.example.fragments_styles_views_threads

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.fragments_styles_views_threads.databinding.ActivityMainBinding
import com.example.fragments_styles_views_threads.databinding.FragmentLoginBinding


class LoginFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment

        val binding = FragmentLoginBinding.inflate(layoutInflater)
        val view = binding.root

        binding.btnSignUp.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.navigateToRegistrationFragment)
        }

        binding.btnLogin.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.navigateToHomeFragment)
        }

        return view
    }

    companion object {

        fun newInstance() = LoginFragment()

    }
}
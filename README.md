# Домашнее задание:
1. Сделать 4 фрагмента
- 1 фрагмент — LoginFragmet
- 2 фрагмент — RegistrationFragment
- 3 фрагмент – HomeFragment
- 4 фрагмент – WebFragment
2. Сделать вёрстку в activity. Сделать Frame под фрагмент. И добавить BottomNavigationView В папке menu сделать xml файл с 3 irems для нашей навигации между фрагментами. В папке navigation создать xml файл navigation_controler, в нём добавить возможность навигации между LoginFragmet и RegistrationFragment, должны быть зациклины, от LoginFragmet сделать навигацию к фрагменту HomeFragment. После чего должен стать видимым BottomNavigationView. В фрагментах регистрации и логина сделать кнопки для переходов, в логине у вас будет 2 кнопки.
